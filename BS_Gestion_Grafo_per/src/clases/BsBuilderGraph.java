/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import clases.Grafo.expansionMinima;
import extras.BsList;
import extras.NBsList;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

class BsBuilderGraph {

    private Grafo grafo = null;//grafo original
    private Grafo grafoExpansion = null;//grafo anterior con expansion minima
    private boolean mostrarSoloExpansion = false;//especifica si solo se muestra el grafo de expansion
    private expansionMinima metodoExpansion = expansionMinima.Kruskal;
    //es decir se colocan invisibles las adyacencias que no fueron elegidas
    Color colorExpansion = Color.RED;
    private Color colorFondo = Color.BLACK;
    //////////////////////
    //////////////////////
    //////////////////////de aqui en Adelante sera toda la parte grafica del grafo
    boolean cargandoArchivo = false;
    Point coordenadasVertice = null;//utilizada en la carga del archivo
    boolean dibujarInmediato = false;
    boolean dibujarCuadricula = false;
    //boolean siempremostrarflecha=false;
    boolean dibujarDirigido = false;
    private JPanel area;//contenedor del juego
    private Image tmp;//temporal para el doble buffer
    private Graphics directo = null;
    private Graphics2D areagrafica = null;//grafico de la imagen interna
//    private boolean graficar=true;
    //////////////////////
    int pesoAdyacenteGrafico = 0;
    private Vertice vertice_en_arrastre = null;
    private Point cvertice_en_arrastre = new Point(0, 0);//COORDENADAS VISUALES INICIALES DE UN VERTICE EN ARRASTRE
    private boolean limitarArrastre = false;
    //////////////////////
    private boolean moverarea = false;//se esta moviendo el area
    private Point cm = null;//coordenada de inicio de mover area
    private Point ci = new Point(0, 0);//estas son las coordenadas de inicio del area grafica;
    private Rectangle cuadroSeleccion = null;
    private boolean en_cuadro_de_seleccion = false;
    private BsList listaVSel = new BsList();//lista de vertices seleccionados
    private Point coorcs = null;//coordenadas de cuadro seleccion
    private final int AJUST_INC = 10;//incremento de tamaño del area de dibujo
    ////////////////////////////////////
    BsList listaResaltados = new BsList();//lista de vertices y adyacencias para resaltar
    private Vertice verticeFinalDijkstra = null;
    private Vertice verticeBajoElMouse = null;
    ////////////////////////////////////
    //////////////////////////////
    Font fv = new java.awt.Font("Arial", Font.BOLD, 20);
    FontMetrics mfv = null;

    public BsBuilderGraph(javax.swing.JPanel contenedor, Grafo grafo) {
        this.grafo = grafo;
        setArea(contenedor);
    }

    void agregar_vertice_grafico(Vertice v) {
        setTamano(v);
        setPosicionRandom(v);
        dibujarVertice(v);
    }

    void setTamano(Vertice v) {
        Rectangle2D medidas = mfv.getStringBounds(v.getNomVer(), areagrafica);
        if (v.getNomVer().trim().equals("")) {
            v.height = 35;
            v.width = 35;
        } else {
            v.height = (int) medidas.getHeight() + 10;
            v.width = (int) medidas.getWidth() + 10;
        }
    }

    private void setPosicionRandom(Vertice v) {
        if (cargandoArchivo) {
            v.setXY(coordenadasVertice.x, coordenadasVertice.y);
        } else {
            do {
                int x = (-ci.x) + (int) ((Math.random() * (area.getWidth() - (v.width))));
                int y = (-ci.y) + (int) ((Math.random() * (area.getHeight() - (v.height))));
                v.setXY(x, y);
            } while (isIntersectVertice(v) == true);
        }
    }

    private void dibujarVertice(Vertice v) {
        if (cargandoArchivo) {
            return;
        }
        areagrafica.setColor(Color.blue);
        areagrafica.fillRect(getCi().x + (int) v.getX(), getCi().y + (int) v.getY(), v.width, v.height);
        if (listaResaltados.contiene(v)) {
            areagrafica.setColor(Color.WHITE);
        } else if (v == vertice_en_arrastre | listaVSel.contiene(v)) {
            areagrafica.setColor(Color.green);
        } else {
            areagrafica.setColor(Color.red);
        }
        areagrafica.fillRect(getCi().x + (int) v.getX() + 2, getCi().y + (int) v.getY() + 2, v.width - 4, v.height - 4);
        if (areagrafica.getColor() == Color.white) {
            areagrafica.setColor(Color.black);
        } else {
            areagrafica.setColor(Color.white);
        }

        areagrafica.setFont(fv);
        areagrafica.drawString(v.getNomVer(), getCi().x + (int) +(v.getCenterX() - (mfv.stringWidth(v.getNomVer()) / 2)), getCi().y + (int) v.getCenterY() + 5);
    }

    synchronized void updateGraphics() {//se crea nuevamente el grafico\
        if (areagrafica != null) {
            areagrafica.setColor(colorFondo);
            areagrafica.fillRect(0, 0, area.getWidth(), area.getHeight());
        }

        boolean previews = dibujarInmediato;
        dibujarInmediato = false;
        if (isDibujarCuadricula()) {
            drawGrid();
        }

        if (grafo.cab != null) {
            Vertice vtmp = null;
//            if (kruskal!=null && mostrarSoloKruskal){//si se está mostrando sólo el kruskal
            //              vtmp=kruskal.cab;
            //        }else {
            vtmp = grafo.cab;
            //      }
            while (vtmp != null) {
                Adyacente atmp = vtmp.lista_ady;
                while (atmp != null) {
                    if (!(vtmp == vinicio && atmp.vertice == vfin)) {
                        agregar_adyacente_grafico(vtmp, atmp.vertice, atmp.dirigido);
                    }
                    atmp = atmp.sig;
                }
                vtmp = vtmp.sig;
            }
            ////dibujo la linea de seleccion de corte de ultimo
            if (vinicio != null) {
                agregar_adyacente_grafico(vinicio, vfin, dirigidaLineaRepintada);
            }
            /////
            vtmp = grafo.cab;
            while (vtmp != null) { //dibujo todos los vertices  
                dibujarVertice(vtmp);
                vtmp = vtmp.sig;
            }
        }
        if (cuadroSeleccion != null) {
            areagrafica.setStroke(new BasicStroke(3));
            areagrafica.setColor(Color.lightGray);
            //coordenada real= -inicio+visual
            //visual=coordenada real +inicio
            areagrafica.drawRect(cuadroSeleccion.x + ci.x, cuadroSeleccion.y + ci.y, cuadroSeleccion.width, cuadroSeleccion.height);
        }

        dibujarInmediato = previews;
    }

    void drawGrid() {
        Font fv2 = new java.awt.Font("Arial", 0, 10);
        int sep = 50;
        areagrafica.setStroke(new BasicStroke(1));
        areagrafica.setColor(Color.PINK);
        int modx = ci.x % sep;
        int mody = ci.y % sep;

        areagrafica.setFont(fv2);
        for (int x = modx; x < area.getWidth(); x += sep) {
            areagrafica.drawLine(x, 0, x, area.getHeight());
            areagrafica.drawString(String.valueOf((-ci.x) + x), x, 10);
            for (int y = mody; y < area.getHeight(); y += sep) {
                areagrafica.drawLine(0, y, area.getWidth(), y);
            }
        }
        for (int y = mody; y < area.getHeight(); y += sep) {
            areagrafica.drawString((-ci.y + y) + "", 0, y);
        }

        areagrafica.drawString(ci.x + "," + ci.y, area.getWidth() / 2, area.getHeight() / 2);
    }

    void repaint() {//se redibuja la imagen guardada
        if (directo == null) {
            return;
        }
        if (dibujarInmediato) {
            directo.drawImage(tmp, 0, 0, null);
        }
    }

    private boolean isIntersectVertice(Vertice v) {
        if (grafo.cab != null) {
            Vertice tcab = grafo.cab;
            while (tcab != null) {
                try {
                    if (tcab != v && v.intersects(tcab)) {
                        return true;
                    }
                } catch (Exception e) {
                }
                tcab = tcab.sig;
            }
        }
        return false;
    }

    private Point getCoordenadasBorde(Vertice v1, Vertice v2) {
        int borde = 3;
        //primero hallo el angulo de inclinacion de la linea
        double tang = Math.abs((v2.getCenterY() - v1.getCenterY()) / (v2.getCenterX() - v1.getCenterX()));
        //System.out.println(v1.nomVer+"→"+v2.nomVer+"→"+tang);
        //long angulo=(long)Math.atan(tang);
        double anchoangulo = ((v2.getHeight() / 2) / tang);
        double var = 0;

        long x2 = 0, y2 = 0;//coordenadas finales
        if (anchoangulo >= (v2.width / 2)) {
            if (v1.getCenterX() <= v2.getCenterX() & v1.getCenterY() >= v2.getCenterY()) {//si el vertice 1 esta al SurOeste
                x2 = (int) (v2.getMinX() - borde);
                var = tang * (v2.getCenterX() - x2);
                y2 = (int) (v2.getCenterY() + var);
                //   System.out.println("mayor suroeste");
            } else if (v1.getCenterX() <= v2.getCenterX() & v1.getCenterY() <= v2.getCenterY()) {//NOROESTE
                x2 = (int) (v2.getMinX() - borde);
                var = tang * (v2.getCenterX() - x2);
                y2 = (int) (v2.getCenterY() - var);
                //  System.out.println("mayor Noroeste");
            } else if (v1.getCenterX() >= v2.getCenterX() & v1.getCenterY() <= v2.getCenterY()) {//NORESTE
                x2 = (int) (v2.getMaxX() + borde);
                var = tang * (x2 - v2.getCenterX());
                y2 = (int) (v2.getCenterY() - var);
                //  System.out.println("mayor NorEste");
            } else {//SurEste
                x2 = (int) (v2.getMaxX() + borde);
                var = tang * (x2 - v2.getCenterX());
                y2 = (int) (v2.getCenterY() + var);
                //  System.out.println("mayor SurEste");
            }
        } else {
            if (v1.getCenterX() <= v2.getCenterX() & v1.getCenterY() >= v2.getCenterY()) {//si el vertice 1 esta al SurOeste
                y2 = (int) (v2.getMaxY() + borde);
                var = ((y2 - v2.getCenterY()) / tang);
                x2 = (int) (v2.getCenterX() - var);
                //  System.out.println("a tamaño SurOeste");
            } else if (v1.getCenterX() <= v2.getCenterX() & v1.getCenterY() <= v2.getCenterY()) {//noroeste
                y2 = (int) (v2.getMinY() - borde);
                var = ((y2 - v2.getCenterY()) / tang);
                x2 = (int) (v2.getCenterX() + var);
                //  System.out.println("a tamaño NorOeste");
            } else if (v1.getCenterX() >= v2.getCenterX() & v1.getCenterY() <= v2.getCenterY()) {//NORESTE
                y2 = (int) (v2.getMinY() - borde);
                var = ((y2 - v2.getCenterY()) / tang);
                x2 = (int) (v2.getCenterX() - var);
                //  System.out.println("a tamaño NorEste");
            } else {//Sur Este
                y2 = (int) (v2.getMaxY() + borde);
                var = ((y2 - v2.getCenterY()) / tang);
                x2 = (int) (v2.getCenterX() + var);
                //  System.out.println("a tamaño SurEste");
            }
        }
        return new Point((int) x2, (int) y2);
    }

    void agregar_adyacente_grafico(Vertice v1, Vertice v2, boolean dirigido) {
        /////////////////
        Adyacente adyacenteActual=v1.buscar_ady(v2.getNomVer());
        double pesoAdyacenteActual=0;
        if (adyacenteActual!=null){
            pesoAdyacenteActual=adyacenteActual.peso;
        }
        /////////////
        //si hay un grafo de expansion minima        
        if (grafoExpansion != null) {
            //y la linea de adyacencia no esta en el grafo de expansion minima
            if (grafoExpansion.isVer2_ady_ver1(v1.getNomVer(), v2.getNomVer())) {
                areagrafica.setColor(colorExpansion);//especifico que la linea tendra el color del metodo de expansion
                //return;
            } else {
                if (mostrarSoloExpansion) {
                    return;
                }
            }
        }
        /////////////
//        if (siempremostrarflecha)dirigido=false;

        Point pf = null;//punto final
        if (dirigido == true) {
            pf = getCoordenadasBorde(v1, v2);
        } else {
            pf = new Point((int) v2.getCenterX(), (int) v2.getCenterY());
        }
        java.awt.Graphics2D g2d = (java.awt.Graphics2D) areagrafica;
        BasicStroke bs = new BasicStroke(1f);
        g2d.setStroke(bs);
        if (areagrafica.getColor() == colorExpansion) {//si el color de dibujo es de la expansion minima es porque esta linea
            //hace parte del grafo de expansion
            bs = new BasicStroke(3f);//dibujo la expansion mas fuerte
            g2d.setStroke(bs);
        } else if (listaResaltados.estaObjeto1AntesObjeto2(v1, v2)) {
            areagrafica.setColor(Color.white);
            bs = new BasicStroke(3f);
            g2d.setStroke(bs);
        } else if (v1 == vinicio && v2 == vfin) {
            areagrafica.setColor(Color.CYAN);
            bs = new BasicStroke(3f);
            g2d.setStroke(bs);
        } else if (dirigido == false) {
            areagrafica.setColor(Color.white);
        } else {
            areagrafica.setColor(Color.green);
        }
        if (v1 == vertice_en_arrastre) {
            areagrafica.setColor(Color.yellow);
        } else if (v2 == vertice_en_arrastre) {
            areagrafica.setColor(Color.ORANGE);
        }

        areagrafica.drawLine(getCi().x + (int) (v1.getCenterX()), getCi().y + (int) (v1.getCenterY()),
                getCi().x + pf.x, getCi().y + pf.y);
        Color color=areagrafica.getColor();
        areagrafica.setColor(Color.CYAN);
        if (areagrafica.getColor() == Color.CYAN) {
            areagrafica.setColor(Color.YELLOW);
            areagrafica.setFont(new Font("Arial black", Font.BOLD, 17));
            FontMetrics mpeso = areagrafica.getFontMetrics();
            peso_adyacencia=pesoAdyacenteActual;
            Rectangle2D medidas = mpeso.getStringBounds((int) peso_adyacencia + "", areagrafica);
            areagrafica.drawString((int) peso_adyacencia + "", (int) ((v1.getCenterX() + v2.getCenterX()) / 2) + ci.x - ((int) (medidas.getWidth() / 2)),
                    (int) ((v1.getCenterY() + v2.getCenterY()) / 2) + ci.y);
        }
        areagrafica.setColor(color);
        areagrafica.setColor(Color.MAGENTA);


        if (dirigido == false) {
            repaint();
            return;
        }

        //Para dibujar la fecha..
        //Ubicamos el final de la flecha fuera del rectangulo
        double ang = 0.0, angSep = 0.0;
        double tx, ty;
        int tamPunta = 0;

        Point punto1 = new Point((int) v1.getCenterX(), (int) v1.getCenterY());
        Point punto2 = new Point(pf.x, pf.y);
        //tamaño de la punta de la flecha
        tamPunta = 15;
        /* (la coordenadas de la ventana es al revez)
         calculo de la variacion de "x" y "y" para hallar el angulo
         **/
        ty = -(punto1.y - punto2.y) * 1.0;
        tx = (punto1.x - punto2.x) * 1.0;
        //angulo
        ang = Math.atan(ty / tx);
        if (tx < 0) {// si tx es negativo aumentar 180 grados
            ang += Math.PI;
        }
        //puntos de control para la punta
        //p1 y p2 son los puntos de salida
        Point p1 = new Point(), p2 = new Point(), punto = punto2;
        //angulo de separacion
        angSep = 20.0;
        p1.x = (int) (punto.x + tamPunta * Math.cos(ang - Math.toRadians(angSep)));
        p1.y = (int) (punto.y - tamPunta * Math.sin(ang - Math.toRadians(angSep)));
        p2.x = (int) (punto.x + tamPunta * Math.cos(ang + Math.toRadians(angSep)));
        p2.y = (int) (punto.y - tamPunta * Math.sin(ang + Math.toRadians(angSep)));
        //dibuja la linea de extremo a extremo
        //g.drawLine(punto1.x, punto1.y, punto.x, punto.y);
        //dibujar la punta
        int[] pun1 = new int[]{getCi().x + punto.x, getCi().x + p1.x, getCi().x + p2.x};
        int[] pun2 = new int[]{getCi().y + punto.y, getCi().y + p1.y, getCi().y + p2.y};
        areagrafica.fillPolygon(pun1, pun2, 3);

        repaint();
    }

    void interfaz_teclaPulsada(int tecla) {
        if (tecla == KeyEvent.VK_DELETE) {
            if (!listaVSel.isVacia()) {
                NBsList v = listaVSel.getPtr();
                while (v != null) {
                    grafo.eliminar_vertice(((Vertice) v.getObj()).getNomVer());
                    v = v.getSig();
                }
                cuadroSeleccion = null;//si doy clic entonces ya no tengo cuadro de seleccion
                listaVSel.vaciar();//vacio la lista de seleccionados
                en_cuadro_de_seleccion = false;
                updateAndRepaint();
            }
        }
    }

    synchronized void interfaz_click_o_iniciarArrastre(int x, int y, int boton) {
        Rectangle cc = new Rectangle((-ci.x) + x, (-ci.y) + y, 10, 10);//un rectangulo con 
        //las coordenadas reales para comprobar con que se intersecta al momento de dar click
        if (boton == 1) {

            if (verticeBajoElMouse != null) {//si hay un vertice debajo del mouse
                if (listaVSel.getTamano() == 1) {//si hay un vertice seleccionado
                    Vertice verticeInicio = (Vertice) listaVSel.getPtr().getObj();
                    if (verticeInicio != verticeBajoElMouse) {//y el seleccionado no es igual al posicionado
                        grafo.dijkstra(verticeInicio, true);
                        verticeFinalDijkstra = verticeBajoElMouse;
                        visualizarDijkstra();//compruebo el resultado
//                        if (!listaResaltados.isVacia()){//si hay ruta
                        updateAndRepaint();//mando a dibujar todo
                        cvertice_en_arrastre.x = x;
                        cvertice_en_arrastre.y = y;
                        vertice_en_arrastre = verticeBajoElMouse;
                        //                      }
                    }
                    return;
                }
            }

            //si tengo un cuadro de seleccion y doy click normal
            if (cuadroSeleccion != null) {
                cuadroSeleccion = null;//si doy clic entonces ya no tengo cuadro de seleccion
                listaVSel.vaciar();//vacio la lista de seleccionados
                en_cuadro_de_seleccion = false;
                updateGraphics();
                repaint();
            }
            /////para iniciar arrastrar un vertice, o para seleccionar destino del Dijkstra
            if (verticeBajoElMouse != null) {
                cvertice_en_arrastre.x = x;
                cvertice_en_arrastre.y = y;
                vertice_en_arrastre = verticeBajoElMouse;
                return;
            } else {
                vertice_en_arrastre = null;
            }
            /*cvertice_en_arrastre.x=x;cvertice_en_arrastre.y=y;
             if (grafo.cab!=null){//si hay vertices
             vertice_en_arrastre=grafo.cab;
             while(vertice_en_arrastre!=null){
             if (vertice_en_arrastre.intersects(cc)){
             if (listaVSel.getTamano()==1){//si hay un vertice seleccionado
             verticeFinalDijkstra=vertice_en_arrastre;
             grafo.dijkstra((Vertice)listaVSel.getPtr().getObj(), true);
             comprobarDijkstra();
             if (!listaResaltados.isVacia()){
             updateAndRepaint();
             vertice_en_arrastre=null;
             }
             }
             return;
             }
             vertice_en_arrastre=vertice_en_arrastre.sig;
             }
             }*/
            /////////para eliminar una linea de adyacencia
            if (vinicio != null && vfin != null) {
                grafo.eliminar_adyacente(vinicio, vfin);
                listaResaltados.vaciar();
                vinicio = vfin = null;
                updateGraphics();
                repaint();
                interfaz_movimientoMouse(x, y);//INVOCO EL MISMO METODO, para que busque otra linea
                //en la misma posicion
                return;
            }
            /////////////////////
            vertice_en_arrastre = null;
            moverarea = true;//SI NO ENTRO EN NADA Y LLEGA A ESTE PUNTO QUIERE DECIR QUE SE VA A EMPEZAR A MOVER
            //EL AREA GRAFICA
            cm = new Point(x, y);//GUARDO LAS COORDENADAS INICIALES DE MOVIDA....
            area.setCursor(new Cursor(Cursor.MOVE_CURSOR));//CAMBIO EL CURSOR
        } else if (boton == 3) {//iniciar cuadro de seleccion
            if (en_cuadro_de_seleccion) {//si el cursor esta en el cuadro de seleccion
                //se empiezan a mover los vertices seleccionados
                //System.out.println("mover seleccion");
                cm = new Point(x, y);
            } else {
                if (cuadroSeleccion != null) {//si se da clic derecho y hay un rectangulo de seleccion
                    if (!(cc.intersects(cuadroSeleccion))) {//y no se da clic dentro del cuadro
                        //se empieza otra seleccion
                        coorcs = new Point((-ci.x) + x, (-ci.y) + y);
                        cuadroSeleccion = new Rectangle(coorcs.x, coorcs.y, 0, 0);
                        listaVSel.vaciar();
                        en_cuadro_de_seleccion = false;
                    }
                } else {
                    //se inicia el arrastre de un cuadro de seleccion
                    //       System.out.println("iniciar seleccion");
                    coorcs = new Point((-ci.x) + x, (-ci.y) + y);
                    cuadroSeleccion = new Rectangle(coorcs.x, coorcs.y, 0, 0);
                }
            }
        }
    }

    void interfaz_arrastre(int x, int y, boolean limitar) {
        limitarArrastre = limitar;

        if (en_cuadro_de_seleccion) {
//        System.out.println("Arrastre en cuadro de seleccion");
            Point copyci = new Point(ci);//copia de las coordenadas de incio
            if (limitar) {
                //cuadro seleccion tiene coordenadas reales, debo converWWtirlas en visuales
                //coordenada real=coordenada visual-inicio
                //coordenada visual=coordenada real+inicio
                //            System.out.println((x-cm.x) + "," + (y-cm.y));
                if (x - cm.x < 0) {//si se mueve a la izquierda
                    if (cuadroSeleccion.x + ci.x + (x - cm.x) < 0) {//si el movimiento hace que la coordenada visual
                        //del cuadro de seleccion sea menor que cero
                        ci.x += AJUST_INC;
                        //x=0;
                        cuadroSeleccion.x = -ci.x;
                    }
                } else if (x - cm.x > 0) {
                    if (cuadroSeleccion.x + ci.x + cuadroSeleccion.width + (x - cm.x) > area.getWidth()) {
                        ci.x -= AJUST_INC;
                        cuadroSeleccion.x = (area.getWidth() - cuadroSeleccion.width) - ci.x;
                        //                    cuadroSeleccion.x=x-ci.x;
                    }
                }
                if (y - cm.y < 0) {
                    if (cuadroSeleccion.y + ci.y + (y - cm.y) < 0) {
                        ci.y += AJUST_INC;
                        //y=0;
                        cuadroSeleccion.y = -ci.y;
                    }
                } else if (y - cm.y > 0) {
                    if (cuadroSeleccion.y + ci.y + cuadroSeleccion.height + (y - cm.y) > area.getHeight()) {
                        ci.y -= AJUST_INC;
                        cuadroSeleccion.y = (area.getHeight() - cuadroSeleccion.height) - ci.y;
                        //                    cuadroSeleccion.y=y-ci.y;
                    }
                }
            }
            Point corrido = new Point();
            //calculo el movimiento de las coordenadas de incio
            //si en algun eje es cero quiere decir que en ese eje no se agrando el area
            //corrido.x=ci.x-copyci.x;
            //corrido.y=ci.y-copyci.y;
            corrido.x = copyci.x - ci.x;
            corrido.y = copyci.y - ci.y;
            if (corrido.x == 0) {//si no hubo movimiento de area en el eje x
                cuadroSeleccion.x += (x - cm.x);//muevo al cuadro de seleccion los puntos de diferencia
                corrido.x = x - cm.x;//y especifico cuanto se corrio
            }
            if (corrido.y == 0) {
                cuadroSeleccion.y += (y - cm.y);
                corrido.y = y - cm.y;
            }
//////////////////////////////////////
            NBsList scab = listaVSel.getPtr();
            while (scab != null) {
                Vertice v = (Vertice) scab.getObj();
                v.setAddXY(corrido.x, corrido.y);
//                Point pv=(Point)scab.getObjrelacion();
                //              scab.setObjrelacion(new Point(pv.x-corrido.x, pv.y-corrido.y));
                scab = scab.getSig();
            }
            cm.x = x;
            cm.y = y;//se especifican las coordenadas del anterior movimiento
            updateGraphics();
            areagrafica.drawString(cuadroSeleccion.x + "," + cuadroSeleccion.y,
                    cuadroSeleccion.x + cuadroSeleccion.width + AJUST_INC + ci.x,
                    cuadroSeleccion.y + ci.y);
            repaint();

        } else if (cuadroSeleccion != null && listaVSel.isVacia()) {//si tengo un cuadro de seleccion, todavia no se han seleccionado los vertices
            //System.out.println("redimensionando cuadro de seleccion");
            if (limitar) {
                if (x < 0) {
                    ci.x += AJUST_INC;
                    x = 0;
                }
                if (y < 0) {
                    ci.y += AJUST_INC;
                    y = 0;
                }
                if (x > area.getWidth()) {
                    ci.x -= AJUST_INC;
                    x = area.getWidth();
                }
                if (y > area.getHeight()) {
                    ci.y -= AJUST_INC;
                    y = area.getHeight();
                }
            }
            //coordenada real=coordenada visual-inicio
            //dibujando cuadro de seleccion
            if (x - ci.x <= coorcs.x) {
                cuadroSeleccion.x = x - ci.x;
            } else {
                cuadroSeleccion.x = coorcs.x;
            }
            cuadroSeleccion.width = Math.abs(x - ci.x - coorcs.x);
            if (y - ci.y <= coorcs.y) {
                cuadroSeleccion.y = y - ci.y;
            } else {
                cuadroSeleccion.y = coorcs.y;
            }
            cuadroSeleccion.height = Math.abs(y - ci.y - coorcs.y);//////////////
            updateAndRepaint();
            /////////////////////////////////////////////////////
        } else if (vertice_en_arrastre != null) {
            //System.out.println("Arrastre vertice");
            int w = vertice_en_arrastre.width / 2;
            int h = vertice_en_arrastre.height / 2;
            if (limitar) {
                //DEBO ajustar las coordenadas visuales iniciales del vertice en arrastre
                //porque estoy moviendo el area grafica
                if (x - w <= 0) {
                    ci.x += AJUST_INC;
                    cvertice_en_arrastre.x += AJUST_INC;
                    x = w;
                }
                if (y - h <= 0) {
                    ci.y += AJUST_INC;
                    cvertice_en_arrastre.y += AJUST_INC;
                    y = h;
                }
                if (x + w >= area.getWidth()) {
                    ci.x -= AJUST_INC;
                    cvertice_en_arrastre.x -= AJUST_INC;
                    x = area.getWidth() - w;
                }
                if (y + h >= area.getHeight()) {
                    ci.y -= AJUST_INC;
                    cvertice_en_arrastre.y -= AJUST_INC;
                    y = area.getHeight() - h;
                }
            }
            vertice_en_arrastre.setXY(x - ci.x - w, y - ci.y - h);
            if (Vertice.adyacenciaPesoReal && grafoExpansion != null) {
                if (metodoExpansion == expansionMinima.Kruskal) {
                    this.setExpansionMinima(grafo.getKruskal());
                } else if (metodoExpansion == expansionMinima.Prim) {
                    this.setExpansionMinima(grafo.getPrim());
                }
            } else {
                updateAndRepaint();
            }
//            arrastre(x-1, y, limitar);
        } else if (moverarea) {
            //System.out.println("Moviendo area");
            ci.x += (x - cm.x);
            ci.y += (y - cm.y);
            cm.x = x;
            cm.y = y;
            updateGraphics();
            repaint();
        }

    }

    void interfaz_terminarArrastre(int x, int y, int boton) {
        area.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        moverarea = false;
        //       System.out.println(listaVSel.isVacia());


        ////////////////////////////////
        Point cics = null;//coordenadas de inicio de cuadro de seleccion ajustadas
        Point cfcs = null;//coordenadas finales del cuadro de seleccion ajustadas
        if (!listaVSel.isVacia()) {//termino un arrastre y no esta la lista vacia
            //guardo las coordenadas iniciales del cuadro de seleccion
            cics = new Point(cuadroSeleccion.x, cuadroSeleccion.y);
            cfcs = new Point(cuadroSeleccion.x + cuadroSeleccion.width, cuadroSeleccion.y + cuadroSeleccion.height);
            //quiere decir que se estaba arrastrando los vertices seleccionados
            //debo verificar si la posicion en donde quedaron no intersecta ningun otro;

            int borde = 10;
            boolean intersectoAlguno = false;
            Vertice v = null;//vertice para comprobar posicion de interseccion
            NBsList lv = listaVSel.getPtr();//lista de vertices seleccionados
            while (lv != null) {//recorre todos los vertices seleccionados
                v = (Vertice) lv.getObj();
                Vertice vc = grafo.cab;
                while (vc != null) {
                    if (v.intersects(vc) && vc != v) {//si el vertice de seleccion intersecta otro vertice
                        //le colocare las coordenadas iniciales
                        intersectoAlguno = true;
                        Point cvi = (Point) lv.getObjrelacion();
                        v.setXY(cvi.x, cvi.y);
                        break;
                    }
                    vc = vc.sig;
                }
                lv.setObjrelacion(new Point((int) v.getX(), (int) v.getY()));
                //lv.setObjrelacion(new Point(v.x, v.y));
                lv = lv.getSig();
            }

            if (intersectoAlguno) {//si alguno en el arrastre intersecto , ajusto el cuadro de selccion
                //reajusto las dimensiones del cuadro de seleccion
                lv = listaVSel.getPtr();//lista de vertices seleccionados
                v = (Vertice) lv.getObj();
                cics = new Point((int) v.getX(), (int) v.getY());
                cfcs = new Point((int) v.getMaxX(), (int) v.getMaxY());
                while (lv != null) {
                    v = (Vertice) lv.getObj();
                    if (v.getX() < cics.x) {
                        cics.x = (int) v.getX();
                    }
                    if (v.getY() < cics.y) {
                        cics.y = (int) v.getY();
                    }
                    if (v.getMaxX() > cfcs.x) {
                        cfcs.x = (int) v.getMaxX();
                    }
                    if (v.getMaxY() > cfcs.y) {
                        cfcs.y = (int) v.getMaxY();
                    }
                    v = v.sig;
                    lv = lv.getSig();
                }
                cuadroSeleccion.x = cics.x - borde;
                cuadroSeleccion.y = cics.y - borde;
                cuadroSeleccion.width = Math.abs(cfcs.x - cics.x) + 2 * borde;
                cuadroSeleccion.height = Math.abs(cfcs.y - cics.y) + 2 * borde;
                updateGraphics();
                repaint();
                ////////////////////////////////
            }
        } else if (cuadroSeleccion != null) {//si hay cuadro de seleccion
            if (grafo.cab != null) {//si hay vertices
                listaVSel.vaciar();
                Vertice v = grafo.cab;

                int bordSel = 10;

                while (v != null) {//comprueba que vertices estan en el cuadro
                    if (v.intersects(cuadroSeleccion)) {
                        listaVSel.insertar(v, new Point((int) v.getX(), (int) v.getY()));//los inserta en la lista de seleccionados
                        //y lo relaciona con las coordenadas de inicio
                        if (listaVSel.getTamano() == 1) {//si se inserto el primero elemento
                            //construyo las coodenadas iniciales del cuadro de seleccion
                            cics = new Point((int) v.getX(), (int) v.getY());
                            cfcs = new Point((int) v.getMaxX(), (int) v.getMaxY());
                        } else {
                            if (v.getX() < cics.x) {
                                cics.x = (int) v.getX();
                            }
                            if (v.getY() < cics.y) {
                                cics.y = (int) v.getY();
                            }
                            if (v.getMaxX() > cfcs.x) {
                                cfcs.x = (int) v.getMaxX();
                            }
                            if (v.getMaxY() > cfcs.y) {
                                cfcs.y = (int) v.getMaxY();
                            }
                        }
                    }
                    v = v.sig;
                }
                if (listaVSel.getTamano() == 0) {//si no se seleccionaron vertices
                    cuadroSeleccion = null;//elimino de memoria el cuadro de seleccion
                } else {
                    //le coloco las coordenadas ajustadas
                    cuadroSeleccion.x = cics.x - bordSel;
                    cuadroSeleccion.y = cics.y - bordSel;
                    cuadroSeleccion.width = Math.abs(cfcs.x - cics.x + 2 * bordSel);
                    cuadroSeleccion.height = Math.abs(cfcs.y - cics.y + 2 * bordSel);
                }
            }
            updateGraphics();
            repaint();
            return;
        }

        if (vertice_en_arrastre == null) {
            return;
        }

        if (grafo.cab != null) {
            Vertice v = grafo.cab;
            while (v != null) {
                if (v != vertice_en_arrastre && v.intersects(vertice_en_arrastre)) {
                    interfaz_arrastre(cvertice_en_arrastre.x, cvertice_en_arrastre.y, false);//el falso
                    //en la linea, quiere decir que no va a limitar el arrastre fina
                    //es decir lo va a colocar sin ajustes de pantalla
//                    if (vertice_en_arrastre.agregar_ady(v, pesoAdyacenteGrafico,dibujarDirigido)){
                    if (grafoExpansion == null || (grafoExpansion != null && !mostrarSoloExpansion)) {
                        if (grafo.agregar_ady(vertice_en_arrastre, v, pesoAdyacenteGrafico, dibujarDirigido)) {
                            //  vertice_en_arrastre.num_sal++;
                            //  v.num_ent++;
                            updateGraphics();
                            repaint();
                        }
                    }
                    vertice_en_arrastre = null;
                    return;
                }
                v = v.sig;
            }
        }
        if (!limitarArrastre) {//sino se esta limitando el arrastre
            interfaz_arrastre(x, y, false);//lo ubica en esta posicion en donde se solto
        }
        vertice_en_arrastre = null;
    }
    Vertice vinicio = null, vfin = null;
    double peso_adyacencia = 0;
    boolean dirigidaLineaRepintada = false;
    boolean repintarLineaPosicionada = false;//especifica que la adyacencia de vinicio a vfin sera
    //colocada con otro grosor de linea

    String infoVertice(int x, int y) {
        Rectangle vc = new Rectangle((-ci.x) + x, (-ci.y) + y, 10, 10);
        Vertice v = null;
        if (grafo.cab != null) {
            v = grafo.cab;
            while (v != null) {
                if (v.intersects(vc)) {
                    verticeBajoElMouse = v;
                    vinicio = null;
                    vfin = null;
                    area.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                    return v.toString();
                }
                v = v.sig;
            }
        }
        verticeBajoElMouse = null;
        return null;
    }

    String interfaz_movimientoMouse(int x, int y) {
////////////////////////////////////////////////////////
        if (listaVSel.getTamano() != 0) {//si hay vertices seleccionados con el cuadro de seleccion
            Rectangle cs = new Rectangle((-ci.x) + x, (-ci.y) + y, 10, 10);
            if (cs.intersects(cuadroSeleccion)) {//si esta en el cuadro de seleccion
                en_cuadro_de_seleccion = true;//lo dejo especificado
                area.setCursor(new Cursor(Cursor.MOVE_CURSOR));//cambio el cursor
                //para que visualice que se puede mover la seleccion
                return listaVSel.getTamano() + " elemento(s) en selección";
            } else {
                en_cuadro_de_seleccion = false;
//                System.out.println("ñao");

            }
        }
///////////////////////////////////////////////////////
        String msg = infoVertice(x, y);//informacion del vertice en esa posicion
        if (msg != null) {
            return msg; //si esta arriba de un vertice no continua con la comprobacion de posicion
        }/////////////////////////////////////////////////////
        vinicio = null;//vertice de inicio
        vfin = null;//vertice final en la flecha de adyacencia

        Vertice v = null;
        if (grafo.cab != null) {
            if (mostrarSoloExpansion) {
                return "";
            }
            v = grafo.cab;
            while (v != null) {
                if (v.lista_ady != null) {
                    Adyacente ady = v.lista_ady;
                    while (ady != null) {
                        double xx = (double) (ady.vertice.getCenterX() - v.getCenterX());
                        double m = 0;
                        if (xx != 0) {//si la posicion en x vertical no es cero
                            m = ((double) (ady.vertice.getCenterY() - v.getCenterY()) / xx);//halla la pendiente
                        }
                        double c = v.getCenterY();
                        //y=mx+c
                        //y-mx-c=0
                        //int resultado=Math.abs((int)(y-(m*(x-v.getCenterX()))-c));
                        //en esta linea de codigo se le resta v.getCenterX() porque debe restar cero
                        //en el primer punto
                        int resultado = 0;
                        if (xx == 0 && (x - ci.x == v.getCenterX())) {
                            resultado = 0;
                        } else {
                            resultado = Math.abs((int) ((-ci.y + y) - (m * ((-ci.x) + x - v.getCenterX())) - c));
                        }
                        //distancia de vertice a vertice
                        double dvv = Math.sqrt(Math.pow(v.getCenterX() - ady.vertice.getCenterX(), 2)
                                + Math.pow(v.getCenterY() - ady.vertice.getCenterY(), 2));
                        //distancia de la posicion al vertice 1
                        double dvp = Math.sqrt(Math.pow(v.getCenterX() - (x - ci.x), 2) + Math.pow(v.getCenterY() - (y - ci.y), 2));
                        //distancia de la posicion al vertice 2
                        double dvp2 = Math.sqrt(Math.pow(ady.vertice.getCenterX() - (x - ci.x), 2) + Math.pow(ady.vertice.getCenterY() - (y - ci.y), 2));
                        /**
                         * *****
                         */
                        if (resultado <= 2 && (dvp < dvv) && (dvp2 < dvv)) {//si esta en una linea
                            vinicio = v;
                            vfin = ady.vertice;
                            dirigidaLineaRepintada = ady.dirigido;
                            peso_adyacencia = ady.peso;
                            Image ima = new javax.swing.ImageIcon(getClass().getResource("/extras/cut_red.png")).getImage();
                            Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(ima, new Point(0, 0), "el que sABE");
                            area.setCursor(cursor);//new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
                            updateAndRepaint();
                            repintarLineaPosicionada = true;
                            return v.getNomVer() + "→" + ady.vertice;
                        }
                        ady = ady.sig;
                    }
                }
                v = v.sig;
            }
            if (repintarLineaPosicionada) {
                repintarLineaPosicionada = false;
                updateAndRepaint();
            }
        }
        area.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        return "";
    }

    private void visualizarDijkstra() {
        listaResaltados.vaciar();//vacio la lista de resultados
        NBsList cabetiqueta = grafo.listEtiquetas.getPtr();//obtengo la cabeza de las etiquetas del Dijkstra
        while (cabetiqueta != null) {
            Etiqueta etiqueta = (Etiqueta) cabetiqueta.getObj();//obtengo la etiqueta en casting
            if (etiqueta.vertice == verticeFinalDijkstra) {//si la etiqueta es de el vertice final
                String verticesCamino[] = etiqueta.ruta.split(",");//obtengo en un vector los nombres de la los vertices
                //de la ruta
                for (int i = 0; i < verticesCamino.length; i++) {//recorro el vector
                    listaResaltados.insertar(grafo.buscar_vertice(verticesCamino[i]));//busco el vertice y lo inserto en la lista de resaltados
//                    System.out.println(verticesCamino[i]);
                }
                break;
            }
            cabetiqueta = cabetiqueta.getSig();
        }
    }

    JPanel getArea() {
        return area;
    }

    void setArea(JPanel area) {
        this.area = area;
        this.tmp = area.createImage(area.getWidth(), area.getHeight());
        this.directo = area.getGraphics();
        this.areagrafica = (Graphics2D) tmp.getGraphics();
        mfv = area.getFontMetrics(fv);
        updateAndRepaint();
    }

    Image getTmp() {
        return tmp;
    }
    BsList objetosrevisado = null;

    void organizar_por_anchura(String nombre) {
        //quito los datos de un cuadro en seleccion
        listaVSel.vaciar();
        cuadroSeleccion = null;
        en_cuadro_de_seleccion = false;
        ///////////////////////
        //Vertice inicio=buscar_vertice(nombre);
        int xr = 0; //es el lugar en x donde va a empezar nuevamente el recorrido
        int xmax = 0;//es el valor de la maxima posicion en donde se ha colocado un vertice

        Vertice inicio = null;
        if (nombre.trim().equals("")) {
            inicio = grafo.cab;
        } else {
            inicio = grafo.buscar_vertice(nombre);
        }
        if (inicio == null) {
            return;
        }
        Point cfila = new Point(0, 0);

        //lista de vertices que llegan;
        BsList listallegados = new BsList();//son los vertices que llegan al vertice que se esta recorriendo
        ///lista de vertices sin relaciones;
        ///lista de vertices sin grado de entrada
        objetosrevisado = new BsList();
        BsList listavertices = new BsList();//necesito una lista de vertices
        listavertices.insertar(inicio);//convierto el Vertice actual en una lista de vertices
        objetosrevisado.insertar(inicio);

        final int incx = 60;//separacion en x
        final int incy = 90;

        Vertice inicio_arco = null;//variable que guarda el primer vertice del arco evaluado
        //se entiene por arco la cantidad de aristas que se encuentra de un nodo inicial

        //esto es el recorrido de un vertice en anchura

        Vertice vlle = null;//este vertice es el primer vertice llegado para continuar la anchura
        //extra despues de terminar la anchura total de un vertice

        do {
            while (!listavertices.isVacia()) {
                Vertice veract = (Vertice) listavertices.extraerPrimero().getObj();
                if (inicio_arco == veract) {
                    cfila.x = xr;
                    cfila.y += incy;
                    veract.setXY(cfila.x, cfila.y);
                    cfila.x += veract.width + incx;
                    inicio_arco = null;

                } else {
                    veract.setXY(cfila.x, cfila.y);
                    cfila.x += veract.width + incx;
                }
                if (cfila.x > xmax) {
                    xmax = cfila.x;
                }

                agregar_llegados(listallegados, veract, objetosrevisado, cfila.y - incy);//agrego en una lista de llegados
                //los llegados de un vertice, si no se encuentran en la otra lista de revisados
                ////////////////////
                Adyacente ady = veract.lista_ady;
                while (ady != null) {
                    if (objetosrevisado.buscar(ady.vertice) == null) {
                        //insertamos en los revisado
                        objetosrevisado.insertar(ady.vertice);
                        System.out.println("No encontrado e Insertado " + ady.vertice.getNomVer());
                        //eliminamos de los llegados\

                        listallegados.eliminar(ady.vertice);
                        listavertices.insertar(ady.vertice);

                        if (inicio_arco == null) {
                            inicio_arco = ady.vertice;
                        }
                    }
                    ady = ady.sig;
                }
            }//en este momento la lista de vertices no tiene objetos
            //es decir que ya termino la anchura del vertice pasado como parametro
            ////////////////////////////////////////////

            NBsList nv = null;
            vlle = null;//no hay vertice insertado porque todavia no se ha extraido
            boolean sw = true;//repeticion activa
            do {
                nv = listallegados.extraerPrimero();//saco el primer Nodo llegado, con el fin de obtener
                //la posicion en y guardada
                if (nv != null) {//si habia nodo
                    vlle = (Vertice) nv.getObj();//obtengo el objeto del nodo
                    cfila.y = (int) nv.getRec();//y la posicion para empezar a colocar el vertice
                    if (objetosrevisado.buscar(vlle) == null) {//si este vertice no ha sido revisado
                        listavertices.insertar(vlle);//lo inserto en la lista de vertices de la anchura actual,
                        //y vendria siendo el primer vertice
                        objetosrevisado.insertar(vlle);//lo inserto en la lista de vertices visitados
                        xr = xmax;
                        cfila.y -= 70;
                        inicio_arco = vlle;
                        sw = false;
                        System.out.println("Vertice  para recorrer ahora " + vlle.getNomVer());
                    }

                } else {
                    sw = false;
                }
            } while (sw == true);//y voy saca de los llegados hasta que el que saque no haya
            //sido revisado
        } while (vlle != null);

        Vertice v = grafo.cab;
        int rec = 0;
        cfila.x = xmax + incx;
        while (v != null) {
            if (v.sinRelaciones()) {
                rec++;
                if (rec == 5) {
                    cfila.y += incx;
                    rec = 0;
                    cfila.x = xmax + incx;
                }
                v.setXY(cfila.x, cfila.y);
                cfila.x += v.width + incx;

            }
            v = v.sig;
        }

        ci.x = 0;
        ci.y = 0;
        updateGraphics();
        repaint();
    }

    private boolean agregar_llegados(BsList listallegados, Vertice v, BsList objetosrevisado, int posy) {
        boolean a = false;
        if (v.lista_lle != null) {
            Adyacente ady = v.lista_lle;
            while (ady != null) {
                if (objetosrevisado.buscar(ady.vertice) == null) {
                    if (listallegados.insertarSinRepetir(ady.vertice, posy)) {
                        System.out.println(ady.vertice.getNomVer());
                    }
                    a = true;//se agrego al menos uno
                }
                ady = ady.sig;
            }
        }
        return a;
    }

    Point getCi() {
        return ci;
    }

    void setCi(Point ci) {
        this.ci = ci;
        updateGraphics();
        repaint();
    }

    void moverXinicial(int vxrelativo) {
        ci.x = ci.x + vxrelativo;
        updateGraphics();
        repaint();
    }

    void moverYinicial(int vyrelativo) {
        ci.y = ci.y + vyrelativo;
        updateGraphics();
        repaint();
    }

    boolean isDibujarCuadricula() {
        return dibujarCuadricula;
    }

    void setDibujarCuadricula(boolean dibujarCuadricula) {
        this.dibujarCuadricula = dibujarCuadricula;
        updateGraphics();
        repaint();
    }

    boolean isDibujarinmediato() {
        return dibujarInmediato;
    }

    void setDibujarinmediato(boolean dibujarinmediato) {
        this.dibujarInmediato = dibujarinmediato;
    }

    void updateAndRepaint() {
        updateGraphics();
        repaint();
    }

    void setExpansionMinima(Grafo expansionMinima) {
        this.grafoExpansion = expansionMinima;
        /*   if (kruskal!=null){
         cambiarColor.start();
         }else{
         cambiarColor.stop();
         updateAndRepaint();
         }*/
        updateAndRepaint();
    }
    javax.swing.Timer cambiarColor = new javax.swing.Timer(1000, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            colorExpansion = new Color((int) (Math.random() * 250), (int) (Math.random() * 250), (int) (Math.random() * 255));
            updateAndRepaint();
        }
    });

    public void setMostrarSoloExpansion(boolean mostrar_solo_expansion) {
        this.mostrarSoloExpansion = mostrar_solo_expansion;
        updateAndRepaint();
    }

    public expansionMinima getMetodoExpansion() {
        return metodoExpansion;
    }

    public void setMetodoExpansion(expansionMinima metodoExpansion) {
        this.metodoExpansion = metodoExpansion;
    }
}
