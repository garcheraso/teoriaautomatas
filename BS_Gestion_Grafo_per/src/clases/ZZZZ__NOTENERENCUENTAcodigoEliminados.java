/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import extras.BsList;
import extras.NBsList;

/**
 *
 * @author BS
 */
public class ZZZZ__NOTENERENCUENTAcodigoEliminados {
//////////
///AMIGO BELSOFT ESTAS INSPIRADO
///SIGUE ASI
    private double menorrecorrido = 0;
    private String posiblesrutas = "";//variable lista

    private String rutas="";
    BsList objetosrevisado = null;//lista de objetos comprobados
    private boolean porpeso;////solo guardo si importa el peso en el camino mas corto

    ///muestra todas las posibles rutas, NO SOLO UNA
    public String caminomascorto(String inicio, String fin, boolean porpeso) {
        this.porpeso = porpeso;
        this.posiblesrutas = "";
        this.rutas="";
        Vertice vi = null;//buscar_vertice(inicio);
        Vertice vf = null;//buscar_vertice(fin);
        if (vi==null || vf==null ){//si alguno de los vertices no existen
            return "Ruta no encontrada";
        }
        //Convierto los vertices en adyacentes para iniciar el proceso
        Adyacente ai=new Adyacente(vi, 0);
        Adyacente af=new Adyacente(vf, 0);
        /////
        objetosrevisado = new BsList();//es la lista de los objetos visitados
        caminomascorto(ai, af, posiblesrutas, 0);

        /////como son variables no de procedimiento sino de clase
        //debo reiniciarlas para que no afecte la siguiente vez que se llame al metodo
        String msg = posiblesrutas;
        posiblesrutas = "";
////
        if (menorrecorrido == 0) {
            menorrecorrido = 0;
            return "Ruta no encontrada";
        } else {
            msg="Recorrido =" + menorrecorrido +"\n"+msg;
            menorrecorrido = 0;
            return msg;
        }
    }
    //la variable ruta es la ruta temporal
    //la variable rec hace referencia a que tanto se ha movido
    private void caminomascorto(Adyacente inicio, Adyacente fin, String ruta, double rec) {
        rutas+="\n"+(ruta+":"+rec);
        NBsList objeto = objetosrevisado.buscar(inicio.vertice);//busco si el objeto ya lo visite

        if (objeto == null) {//si no he visitado el objeto
            objetosrevisado.insertar(inicio.vertice, rec);//lo inserto en los visitados con el numero de recorrido de visita
        } else {//ya fue visitado
            if (objeto.getRec()!=0 && rec < objeto.getRec()) {//si el recorrido actual es menor al del registro de la visita
                objeto.setRec(rec);//le cambio el numero de visita
            } else {
                return;//salgo del metodo para no seguir comparando, queremos menor ruta
            }
        }

        if (ruta.equals("")) {
            ruta = inicio.vertice.getNomVer();//si no hay ruta de recorrido
        }        //le coloca el inicio de la ruta

        double pesoagregado=0;

        Adyacente ady = inicio.vertice.lista_ady;//obtengo la lista de adyacencia del provisional vertice de inicio
        while (ady != null) {
        ///////////////////////
            if (porpeso){
                pesoagregado=ady.peso;
            } else pesoagregado=1;

            ////////////////////////
            if (ady.vertice == fin.vertice) {
                if (menorrecorrido == 0) {//si el menor recorrido es cero entonces no se ha encontrado un recorrido
                    menorrecorrido=rec+pesoagregado;
                    posiblesrutas = ruta + "→" + ady.vertice.getNomVer();//se guarda una ruta
                } else {// ya se encontro algun recorrido
                    if (menorrecorrido == rec+pesoagregado) {//y tiene iguales movimientos al anterior encontrado
                        posiblesrutas += "\n" + ruta + "→" + ady.vertice.getNomVer();//añado otro ruta a la lista
                    } else if (rec+pesoagregado < menorrecorrido) {//si es menor que el recorrido menor
                        menorrecorrido=rec+pesoagregado;
                        posiblesrutas = ruta + "→" + ady.vertice.getNomVer();//se guarda una ruta
                    }
                }
                rutas+="\n"+(ruta+"→"+ady.vertice.getNomVer()+":"+(rec+pesoagregado));
                //ady=null;
                break;//salimos del recorrido de adyacentes
            } else {

                if (menorrecorrido != 0) {//si se ha encontrado un recorrido
                    if (rec+pesoagregado > menorrecorrido) {//y este recorrido es mayor
                        rutas+="\n"+(ruta+"→"+ady.vertice.getNomVer()+":"+(rec+pesoagregado));
                        return;//no es recomendable hacer la recursividad
                    }
                }
                //invoco el metodo recursivo con el peso agregado mas
                caminomascorto(ady, fin, ruta + "→" + ady.vertice.getNomVer(), rec + pesoagregado);
                ady = ady.sig;

            }
        }
    }

}
