package extras;

////BS BUILDER.......
public class BsList {
//Esta clase es para manejar conjunto de controles
    NBsList ptr;
    NBsList fin;
    private int tamano=0;

    public boolean isVacia(){
        return ptr==null;
    }
    public BsList() {
    }
    public boolean eliminar(Object obj){
        if (ptr!=null){
           NBsList nl=ptr;
           while(nl!=null){
               if (nl.getObj()==obj){
                   if (ptr.getSig()==null){
                       ptr=null;
                       fin=null;
                   }else if(ptr.getSig()!=null){
                       ptr=ptr.getSig();
                       ptr.setAnt(null);
                   }else if (nl==fin){
                       fin.getAnt().setSig(null);
                       fin=fin.getAnt();
                   }else{
                       nl.getAnt().setSig(nl.getSig());
                       nl.getSig().setAnt(nl.getAnt());
                   }
                   tamano--;
                   return true;
               }
               nl=nl.getSig();
           }
        }
        return false;
    }
    public NBsList buscar (Object obj){
        if (obj==null)return null;

        if (ptr!=null){
            NBsList act=ptr;
            while(act!=null){
                if (act.getObj()==obj){
                   return act;
                }
                act=act.getSig();
            }
        }
        return null;
    }
    public NBsList buscarbyRel(Object obj){
        if (obj==null)return null;

        if (ptr!=null){
            NBsList act=ptr;
            while(act!=null){
                if (act.getObjrelacion()==obj){
                   return act;
                }
                act=act.getSig();
            }
        }
        return null;
    }
    public boolean eliTodosContenganRelacion(Object obj){
        if (ptr!=null){
            NBsList nl=getPtr();
            while(nl!=null){
                if (nl.getObjrelacion()==obj){
                   if (ptr.getSig()==null){
                       ptr=null;
                       fin=null;
                       nl=null;continue;
                   }else if(ptr.getSig()!=null){
                       ptr=ptr.getSig();
                       ptr.setAnt(null);
                       nl=ptr;continue;
                   }else if (nl==fin){
                       fin.getAnt().setSig(null);
                       fin=fin.getAnt();
                   }else{
                       nl.getAnt().setSig(nl.getSig());
                       nl.getSig().setAnt(nl.getAnt());
                   }
                   tamano--;
                }
                nl=nl.getSig();
            }
        }
        return false;
    }
    public boolean contiene(Object obj){
        return !(null==buscar(obj));
    }
    public boolean contieneRelacion(Object obj){
        return !(null==buscarbyRel(obj));
    }
    public boolean contieneGrupo(Object obj, Object objRel){
        NBsList n=buscar(obj);
        if (n==null)return false;
        if (n.getObjrelacion()==objRel){
            return true;
        }else{
            return false;
        }
    }
    public boolean estaObjeto1AntesObjeto2(Object obj, Object obj2){
        NBsList n=buscar(obj);
        if (n!=null){
            if (n.getSig()!=null){
                if (n.getSig().getObj()==obj2){
                    return true;
                }
            }
        }
        return false;
    }
    public NBsList extraerPrimero(){
        if (ptr!=null){
            NBsList objeto=ptr;
            ptr=ptr.getSig();
            if (ptr==null)fin=null;
            tamano--;
            return objeto;
        }
        return null;
    }
    public void insertar(Object obj, Object objrelacion){
        NBsList nl=new NBsList(obj,objrelacion);
        if (ptr==null){
            ptr=nl;
            fin=nl;
        }else{
            fin.setSig(nl);
            nl.setAnt(fin);
            fin=fin.getSig();
        }
        tamano++;
    }
    public void insertar(Object obj, int rec){
        NBsList nl=new NBsList(obj,rec);
        if (ptr==null){
            ptr=nl;
            fin=nl;
        }else{
            fin.setSig(nl);
            nl.setAnt(fin);
            fin=fin.getSig();
        }
        tamano++;
    }
    public void insertar(Object obj){
        insertar(obj,1);
    }
    public void insertarSinRepetir(Object obj){
        insertarSinRepetir(obj,1);
    }
    public boolean insertarSinRepetir(Object obj, int rec){
        NBsList b=buscar(obj);
        if (b==null){//si no existe el objeto
            insertar(obj,rec);
            return true;
        }else{
            return false;
        }
    }    
    public void insertarPorOrdenRec(Object obj,Object obj2, double rec, boolean estado){
        //inserta objetos ordenados por una variable de recorrido
        if (obj==null && obj2==null)return;
            NBsList nuevo=new NBsList(obj, obj2, rec, estado);
        if (ptr==null){
            ptr=nuevo;
            fin=nuevo;
        }else{
            NBsList tcab=getPtr();
            while(tcab!=null){
                if (nuevo.getRec()<=tcab.getRec()){//si el nodo es menor que el del recorrido
                    if (tcab==ptr){
                        nuevo.setSig(ptr);
                        ptr.setAnt(nuevo);
                        ptr=nuevo;
                        break;
                    }else{
                        if (tcab.getAnt().getRec()<=nuevo.getRec()){
                            tcab.getAnt().setSig(nuevo);
                            nuevo.setAnt(tcab.getAnt());
                            nuevo.setSig(tcab);
                            tcab.setAnt(nuevo);
                            break;
                        }
                    }
                }
                tcab=tcab.getSig();
            }
            if (tcab==null){
                fin.setSig(nuevo);
                nuevo.setAnt(fin);
                fin=nuevo;
            }
        }
        tamano++;
    }
    public NBsList getPtr(){
        return ptr;
    }
    public void setPtr(NBsList nuevoPtr){
        if (nuevoPtr!=null){
            ptr=nuevoPtr;
            nuevoPtr.setAnt(null);
            NBsList rec=ptr;
            tamano=0;
            while(rec!=null){
                tamano++;
                if (rec.getSig()==null)fin=rec;
                rec=rec.getSig();
            }
        }
    }
    public NBsList getFin(){
        return fin;
    }
    public void setFin(NBsList nuevoFin){
        if (nuevoFin!=null){
            fin=nuevoFin;
            fin.setSig(null);
            NBsList rec=fin;
            tamano=0;
            while(rec!=null){
                tamano++;
                if (rec.getAnt()==null)ptr=rec;
                rec=rec.getAnt();
            }
        }
    }
    public Object getObject(int pos){
        NBsList ret=null;
        if (ptr!=null){
            if (ptr.getSig()==null){
                return ptr.getObj();
            }else{
                NBsList tmptr=ptr;
                int rec=0;
                do{
                    rec++;
                    if (rec==pos){
                        return tmptr.getObj();
                    }
                    tmptr=tmptr.getSig();
                }while(tmptr!=null);
            }
        }
        return ret;
    }
    public Object getObjectCasting(int pos){
        NBsList nl=(NBsList)getObject(pos);
        return nl.getObj();
    }

    @Override
    public String toString() {
        String cad="";
        NBsList n=ptr;
        while(n!=null){
            cad+=n.getObj().toString()+"\n";
            n=n.getSig();
        }
        return cad;
    }
    public String toString2() {
        String cad="";
        NBsList n=ptr;
        while(n!=null){
            cad+=n.getObjrelacion().toString()+"\n";
            n=n.getSig();
        }
        return cad;
    }
    public String toString3(){
        String cad="";
        NBsList n=ptr;
        while(n!=null){
            cad+=n.getRec()+"\n";
            n=n.getSig();
        }
        return cad;
    }
    public int getTamano() {
        return tamano;
    }
    public void vaciar(){
        ptr=null;
        fin=null;
        this.tamano=0;
    }

}
