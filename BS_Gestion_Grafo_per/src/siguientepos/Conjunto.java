/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siguientepos;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BS
 */
public class Conjunto {

    String nombre = "";
    List<Integer> hojas = new LinkedList<>();
    private boolean evaluado=false;

    public boolean esVacio() {
        return hojas.isEmpty();
    }

    public Conjunto(String nombre) {
        this.nombre = nombre;
    }

    public boolean contiene(Integer[] numero) {
        for (Integer numero1 : numero) {
            if (hojas.indexOf(numero1) != -1) {
                return true;
            }
        }
        return false;
    }

    public void setHojas(Integer[] numeroDeHoja) {
        hojas.clear();
        hojas.addAll(Arrays.asList(numeroDeHoja));
        Collections.sort(hojas);
    }
    public void setHojas(List<Integer> numeroDeHoja){
        hojas.clear();
        hojas.addAll(numeroDeHoja);
        Collections.sort(hojas);
    }

    public Conjunto(String caracter, Integer[] numeroDeHoja) {
        this.nombre = caracter;
    }

    public void agregarHoja(Integer hoja) {
        if (hojas.indexOf(hoja) == -1) {
            hojas.add(hoja);
        }
    }

    public void agregarHoja(List<Integer> hoja) {
        for (Integer h : hoja) {
            if (hojas.indexOf(h) == -1) {
                hojas.add(h);
            }
        }
    }

    public void ordenar() {
        Collections.sort(hojas);
    }

    @Override
    public String toString() {
        return nombre + "={" + AFDOptimoEdgar.vectorACadena(hojas.toArray(), ",") + "}";
    }
    public String mostrarConjunto(){
        return "" + AFDOptimoEdgar.vectorACadena(hojas.toArray(), ",") + "";        
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Conjunto) {
            Conjunto conjunto = (Conjunto) obj;
            if (conjunto.hojas.size() == this.hojas.size()) {
                for (Integer elemento1 : conjunto.hojas) {
                    if (this.hojas.indexOf(elemento1) == -1) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isEvaluado() {
        return evaluado;
    }

    public void setEvaluado(boolean evaluado) {
        this.evaluado = evaluado;
    }

}
