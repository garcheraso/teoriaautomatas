/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siguientepos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BS
 */
public class AFDOptimo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hoja concatenacion[] = new Hoja[4];
        concatenacion[0] = new Hoja("M", new Integer[]{1, 4, 7, 11, 14, 18, 21, 26, 30, 37, 41});
        concatenacion[1] = new Hoja("m", new Integer[]{5, 8, 12, 15, 19, 22, 27, 31, 38, 42});
        concatenacion[2] = new Hoja("d", new Integer[]{3, 6, 10, 13, 17, 20, 25, 29, 36, 40});
        concatenacion[3] = new Hoja("_", new Integer[]{2, 23, 24, 28, 33, 34, 35, 37, 39});
        SiguientePos siguiente[] = new SiguientePos[43 + 1];
        siguiente[1] = new SiguientePos(new Integer[]{2, 3, 4, 5});
        siguiente[2] = new SiguientePos(new Integer[]{3, 4, 5});
        siguiente[3] = new SiguientePos(new Integer[]{6, 7, 8, 10, 11, 12});
        siguiente[4] = new SiguientePos(new Integer[]{6, 7, 8, 10, 11, 12});
        siguiente[5] = new SiguientePos(new Integer[]{6, 7, 8, 10, 11, 12});
        siguiente[6] = new SiguientePos(new Integer[]{6, 7, 8});
        siguiente[7] = new SiguientePos(new Integer[]{6, 7, 8});
        siguiente[8] = new SiguientePos(new Integer[]{6, 7, 8});
        siguiente[9] = new SiguientePos(new Integer[]{});
        siguiente[10] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[11] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[12] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[13] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[14] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[15] = new SiguientePos(new Integer[]{13, 14, 15, 17, 18, 19});
        siguiente[16] = new SiguientePos(new Integer[]{});
        siguiente[17] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[18] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[19] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[20] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[21] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[22] = new SiguientePos(new Integer[]{20, 21, 22, 23});
        siguiente[23] = new SiguientePos(new Integer[]{24, 25, 26, 27, 33});
        siguiente[24] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[25] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[26] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[27] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[28] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[29] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[30] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[31] = new SiguientePos(new Integer[]{28, 29, 30, 31, 33});
        siguiente[32] = new SiguientePos(new Integer[]{});
        siguiente[33] = new SiguientePos(new Integer[]{34, 35, 36, 37, 38});
        siguiente[34] = new SiguientePos(new Integer[]{34, 35, 36, 37, 38});
        siguiente[35] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[36] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[37] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[38] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[39] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[40] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[41] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[42] = new SiguientePos(new Integer[]{39, 40, 41, 42, 43});
        siguiente[43] = new SiguientePos(new Integer[]{});
        List<Conjunto> conjuntos = new LinkedList();
        int numConjunto = 1;
        int numConjuntoAEvaluar = 1;
        Conjunto conjuntoPrincipal = new Conjunto("C" + numConjunto);
        conjuntoPrincipal.setHojas(new Integer[]{1, 23});

        Conjunto conjuntoEvaluando = conjuntoPrincipal;
        boolean sw = true;
        System.out.println("ppos(raiz)=" + conjuntoEvaluando);

        ////////
        Object matriz[][] = new Object[100][concatenacion.length + 1];
        ////////
        do {
            System.out.println("------");
            System.out.println(conjuntoEvaluando);
            matriz[numConjuntoAEvaluar][0] = conjuntoEvaluando.nombre;
            for (int i = 0; i < concatenacion.length; i++) {
                System.out.append("Trans(" + conjuntoEvaluando.nombre + "," + concatenacion[i].caracter + ")={");
                Conjunto conjuntoTemporal = new Conjunto(null);
                for (Integer hoja : conjuntoEvaluando.hojas) {
                    conjuntoTemporal.agregarHoja(interseccion(siguiente[hoja].siguiente, concatenacion[i].hojas));
                }
                conjuntoTemporal.ordenar();
                System.out.append(vectorACadena(conjuntoTemporal.hojas.toArray(), ","));
                System.out.append("}=");
                int indexBusqueda = conjuntos.indexOf(conjuntoTemporal);
                if (indexBusqueda == -1) {//Si no existe el conjunto
                    numConjunto++;
                    conjuntoTemporal.nombre = "C" + numConjunto;
                    conjuntos.add(conjuntoTemporal);
                } else {
                    conjuntoTemporal = conjuntos.get(indexBusqueda);
                }
                matriz[numConjuntoAEvaluar][i+1] = conjuntoTemporal.nombre;
                System.out.append(conjuntoTemporal.nombre);
                System.out.append("\n");
            }
            numConjuntoAEvaluar++;
            if (numConjuntoAEvaluar <= conjuntos.size()+1) {
                conjuntoEvaluando = conjuntos.get(numConjuntoAEvaluar - 2);
            } else {
                sw = false;
            }
        } while (sw);

        System.out.println("Matriz de adyacencia");
        for (int i = 0; i < numConjuntoAEvaluar; i++) {
            if (i == 0) {
                for (int k = 0; k < concatenacion.length + 1; k++) {
                    if (k > 0) {
                        System.out.append("|");
                    }
                    if (k == 0) {

                    } else {
                        System.out.append(concatenacion[k - 1].caracter);
                    }
                }
                System.out.append("\n");
            } else {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (j == 0 && i == 0) {
                        continue;
                    }
                    if (j > 0) {
                        System.out.append("|");
                    }
                    System.out.append(matriz[i][j] == null ? "null" : matriz[i][j].toString());
                }
                System.out.append("\n");
            }
        }
        System.out.println("");
        for (Conjunto con: conjuntos) {
            if (con.contiene(new Integer[]{35,36,37,38,39,40,41,42})){
                System.out.append(con.nombre+",");
            }
        }
    }

    public static String vectorACadena(Object[] lista, String separador) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lista.length; i++) {
            if (sb.length() != 0) {
                sb.append(separador);
            }
            sb.append(lista[i].toString());
        }
        return sb.toString();
    }

    public static List<Integer> interseccion(List<Integer> lista1, List<Integer> lista2) {
        Conjunto cojunto = new Conjunto("");
        for (Integer elemento1 : lista1) {///Se verifica cuales elementos de la lista1 se encuentran en la lista2
            if (lista2.indexOf(elemento1) != -1) {
                cojunto.agregarHoja(elemento1);
            }
        }
        return cojunto.hojas;

    }

}
