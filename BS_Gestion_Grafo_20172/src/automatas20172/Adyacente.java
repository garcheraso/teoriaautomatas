package automatas20172;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Adyacente {

    private Vertice vertice;//cambio en la clase para el manejo de punteros de memoria
    Set<String> transiciones = new LinkedHashSet<>();

    public Adyacente(Vertice vertice, String transiciones) {
        this.vertice=vertice;
        String[] vectorTransicion = transiciones.split(" ");
        this.transiciones.addAll(Arrays.asList(vectorTransicion));
    }
    public void agregarTransiciones(String transiciones){
        String[] vectorTransicion = transiciones.split(" ");
        this.transiciones.addAll(Arrays.asList(vectorTransicion));
    }
    
    @Override
    public String toString() {
        return transiciones.stream().collect(Collectors.joining(" ")) + "→" + getVertice().nomVer;
    }

    public Vertice getVertice() {
        return vertice;
    }
    
}
