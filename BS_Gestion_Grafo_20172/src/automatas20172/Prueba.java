package automatas20172;

import java.util.Arrays;

/**
 *
 * @author BS
 */
public class Prueba {

    public static void main(String[] args) {
        ///Alt +28∟
        Grafo grafo = new Grafo();
//        grafo.agregar_ady("q0",true ,"q1",false, "a b");
//        grafo.agregar_ady("q1", false, "q2",true, "a b");
//        grafo.agregar_ady("q1", false, "q3", true, "∟");
//        grafo.agregar_ady("q1", "q4", "b");
//        grafo.agregar_ady("q2", "q3", "c a");
//        grafo.agregar_ady("q4", "q3", "a");
        grafo.agregar_ady("q0", true, "q1", false, "∟");
        grafo.agregar_ady("q0", false, "q3", true, "b");
        grafo.agregar_ady("q0", false, "q4", true, "c");
        grafo.agregar_ady("q1", "q2", "a");
        grafo.agregar_ady("q1", "q5", "∟");
        grafo.agregar_ady("q2", false, "q4",true, "a");
        grafo.agregar_ady("q3", "q2", "a");
        grafo.agregar_ady("q4", "q3", "b");
        grafo.agregar_ady("q5", "q2", "b");
        grafo.agregar_ady("q5", "q4", "a");

        System.out.println(grafo);
        Arrays.asList("aab","bca", "aabbca", "cbaabaabaa", "aaba").forEach(
                s -> System.out.println("Resultado " + s + " " + grafo.evaluarCadena(s))
        );
    }
}
