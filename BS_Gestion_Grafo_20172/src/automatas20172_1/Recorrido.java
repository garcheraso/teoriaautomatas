/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas20172_1;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author BS
 */
public class Recorrido {

    List<Transicion> recorrido = new LinkedList();

    public Recorrido(String transicion, Estado estadoSiguiente){
        this(new Transicion(estadoSiguiente, transicion));
    }
    public Recorrido(Transicion transicion) {
        this.recorrido.add(transicion);
    }

    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Recorrido) {
            Recorrido r = (Recorrido) obj;
            if (this.recorrido.size() == r.recorrido.size()) {
                return recorrido.stream().allMatch((t) -> (r.recorrido.contains(t)));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.recorrido);
        return hash;
    }

    public boolean isValido() {
        if (recorrido.isEmpty()) {
            return false;
        } else {
            return recorrido.get(recorrido.size() - 1).getEstadoSiguiente().isFinal();
        }
    }

}
