package automatas20172_1;

/**
 *
 * @author BS
 */
public class Prueba {

    public static void main(String[] args) {
        ///Alt +28∟
        Grafo grafo = new Grafo();
        grafo.agregarEstadoTransicion("q0", true, "q1", true, "a");
        grafo.agregarEstadoTransicion("q0", true, "q2", false, "b");
        grafo.agregarEstadoTransicion("q1", false, "q2", false, "b");
        grafo.agregarEstadoTransicion("q2", "q1", "c");
        grafo.agregarEstadoTransicion("q2", false, "q3",true, "a");
        grafo.agregarEstadoTransicion("q3", "q2", "b");
        grafo.agregarEstadoTransicion("q3", "q1", "d");

        System.out.println(grafo);
//        Arrays.asList("aab", "bca", "aabbca", "cbaabaabaa", "aaba").forEach(
//                s -> System.out.println("Resultado " + s + " " + grafo.evaluarCadena(s))
//        );
//
//        List<String> lista = new LinkedList<>();
//        lista.add("a");
//        lista.add("b");
//        lista.add("c");
//        ListIterator<String> it = lista.listIterator();
//        while (it.hasNext()) {
//            String c = it.next();
//            if (!c.equals("c")) {
//            }
//            it.add("d");
//            System.out.println(c);
//        }
//        System.out.println(lista);
//        System.out.println("-----");
        grafo.posiblesProducciones();

    }

    public static void evaluarMatriz(String[][] matriz, String cadena) {
        Grafo grafo = new Grafo();
        for (int fila = 1; fila < matriz.length; fila++) {
            for (int columna = 1; columna < matriz[fila].length; columna++) {
                grafo.agregarEstadoTransicion(matriz[fila][0], matriz[0][columna], matriz[fila][columna]);
            }
        }
        System.out.println("Evaluar " + cadena + "->" + grafo.evaluarCadena(cadena));
    }

}
