/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siguientepos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BS
 */
public class AFDOptimoRobertVargas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hoja concatenacion[] = new Hoja[12];
        concatenacion[0] = new Hoja("1", new Integer[]{1, 12});
        concatenacion[1] = new Hoja("2", new Integer[]{3});
        concatenacion[2] = new Hoja("c", new Integer[]{2});
        concatenacion[3] = new Hoja("d", new Integer[]{4});
        concatenacion[4] = new Hoja("s", new Integer[]{5,14});
        concatenacion[5] = new Hoja("m", new Integer[]{6,15});
        concatenacion[6] = new Hoja("n", new Integer[]{7,16});
        concatenacion[7] = new Hoja("0", new Integer[]{9});
        concatenacion[8] = new Hoja("a", new Integer[]{11});
        concatenacion[9] = new Hoja("b", new Integer[]{13});
        concatenacion[10] = new Hoja("", new Integer[]{8,10,17,19});
        concatenacion[11] = new Hoja("T", new Integer[]{18});
        //Numero de hojas totales
        int numeroHojas = 20;
        SiguientePos siguiente[] = new SiguientePos[numeroHojas + 1];
        siguiente[1] = new SiguientePos(new Integer[]{2});
        siguiente[2] = new SiguientePos(new Integer[]{5});
        siguiente[3] = new SiguientePos(new Integer[]{4});
        siguiente[4] = new SiguientePos(new Integer[]{5});
        siguiente[5] = new SiguientePos(new Integer[]{6});
        siguiente[6] = new SiguientePos(new Integer[]{7});
        siguiente[7] = new SiguientePos(new Integer[]{9,12,20});
        siguiente[8] = new SiguientePos(new Integer[]{});
        siguiente[9] = new SiguientePos(new Integer[]{11});
        siguiente[10] = new SiguientePos(new Integer[]{});
        siguiente[11] = new SiguientePos(new Integer[]{14});
        siguiente[12] = new SiguientePos(new Integer[]{13});
        siguiente[13] = new SiguientePos(new Integer[]{18});
        siguiente[14] = new SiguientePos(new Integer[]{15});
        siguiente[15] = new SiguientePos(new Integer[]{16});
        siguiente[16] = new SiguientePos(new Integer[]{18});
        siguiente[17] = new SiguientePos(new Integer[]{});
        siguiente[18] = new SiguientePos(new Integer[]{20});
        siguiente[19] = new SiguientePos(new Integer[]{});
        siguiente[20] = new SiguientePos(new Integer[]{});

        List<Conjunto> conjuntos = new LinkedList();
        int numConjunto = 1;
        int numConjuntoAEvaluar = 1;
        Conjunto conjuntoPrincipal = new Conjunto("C" + numConjunto);
        conjuntoPrincipal.setHojas(new Integer[]{1, 2});

        Conjunto conjuntoEvaluando = conjuntoPrincipal;
        boolean sw = true;
        System.out.println("ppos(raiz)=" + conjuntoEvaluando);

        ////////
        Object matriz[][] = new Object[100][concatenacion.length + 1];
        ////////
        List<Integer> inter;
        do {
            System.out.println("------");
            System.out.println(conjuntoEvaluando);
            matriz[numConjuntoAEvaluar][0] = conjuntoEvaluando.nombre;
            for (int i = 0; i < concatenacion.length; i++) {
                System.out.append("Trans(" + conjuntoEvaluando.nombre + "," + concatenacion[i].caracter + ")={");
                Conjunto conjuntoTemporal = new Conjunto(null);
                for (Integer hoja : conjuntoEvaluando.hojas) {
                    inter = interseccion(siguiente[hoja].siguiente, concatenacion[i].hojas);
                    if (inter.size() > 0) {
                        conjuntoTemporal.agregarHoja(inter);
                    }
                }
                conjuntoTemporal.ordenar();
                System.out.append(vectorACadena(conjuntoTemporal.hojas.toArray(), ","));
                System.out.append("}");
                int indexBusqueda = conjuntos.indexOf(conjuntoTemporal);
                if (indexBusqueda == -1) {
                    if (!conjuntoTemporal.esVacio()) {
                        numConjunto++;
                        conjuntoTemporal.nombre = "C" + numConjunto;
                        conjuntos.add(conjuntoTemporal);
                    }
                } else {
                    conjuntoTemporal = conjuntos.get(indexBusqueda);
                }
                if (conjuntoTemporal.esVacio()) {
                    matriz[numConjuntoAEvaluar][i + 1] = "";
                }
                matriz[numConjuntoAEvaluar][i + 1] = conjuntoTemporal.nombre;
                if (conjuntoTemporal.nombre != null) {
                    System.out.append("=").append(conjuntoTemporal.nombre);
                }
                System.out.append("\n");
            }
            numConjuntoAEvaluar++;
            if (numConjuntoAEvaluar <= conjuntos.size() + 1) {
                conjuntoEvaluando = conjuntos.get(numConjuntoAEvaluar - 2);
            } else {
                sw = false;
            }
        } while (sw);

        System.out.println("Matriz de adyacencia");
        for (int i = 0; i < numConjuntoAEvaluar; i++) {
            if (i == 0) {
                for (int k = 0; k < concatenacion.length + 1; k++) {
                    if (k > 0) {
                        System.out.append("|");
                    }
                    if (k == 0) {

                    } else {
                        System.out.append(concatenacion[k - 1].caracter);
                    }
                }
                System.out.append("\n");
            } else {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (j == 0 && i == 0) {
                        continue;
                    }
                    if (j > 0) {
                        System.out.append("|");
                    }
                    System.out.append(matriz[i][j] == null ? "null" : matriz[i][j].toString());
                }
                System.out.append("\n");
            }
        }
        System.out.println("");
        /*for (Conjunto con : conjuntos) {
         if (con.contiene(new Integer[]{35, 36, 37, 38, 39, 40, 41, 42})) {
         System.out.append(con.nombre + ",");
         }
         }*/
    }

    public static String vectorACadena(Object[] lista, String separador) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lista.length; i++) {
            if (sb.length() != 0) {
                sb.append(separador);
            }
            sb.append(lista[i].toString());
        }
        return sb.toString();
    }

    public static List<Integer> interseccion(List<Integer> lista1, List<Integer> lista2) {
        Conjunto cojunto = new Conjunto("");
        for (Integer elemento1 : lista1) {///Se verifica cuales elementos de la lista1 se encuentran en la lista2
            if (lista2.indexOf(elemento1) != -1) {
                cojunto.agregarHoja(elemento1);
            }
        }
        return cojunto.hojas;

    }
    
    public static void iniciarAnalisisAFDNoOptimo(){
    }

}
