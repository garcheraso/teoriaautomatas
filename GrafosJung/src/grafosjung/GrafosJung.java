/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosjung;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.AggregateLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.LayoutDecorator;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.layout.CachingLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;
import javax.swing.JFrame;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.apache.commons.collections15.functors.CloneTransformer;

/**
 *
 * @author BS
 */
public class GrafosJung {

    static int edgeCount = 0;
    static Graph<Vertice, String> g = new SparseMultigraph();
    static Vertice v1 = new Vertice("1");
    static Vertice v2 = new Vertice("2");
    static Vertice v3 = new Vertice("5");
    static Vertice v4 = new Vertice("4");
    static Vertice v5 = new Vertice("4");
    static Vertice v6 = new Vertice("6");
    static Vertice v7 = new Vertice("7");

    public static void main(String[] args) {

        g.addEdge("1-1", v1, v1, EdgeType.DIRECTED);
        g.addEdge("1-2", v1, v2, EdgeType.DIRECTED);
        g.addEdge("1-3", v1, v3, EdgeType.DIRECTED);
        g.addEdge("2-3", v2, v3, EdgeType.DIRECTED);
        g.addEdge("2-4", v2, v4, EdgeType.DIRECTED);
        g.addEdge("3-5", v3, v5, EdgeType.DIRECTED);
        g.addEdge("3-4", v3, v5, EdgeType.DIRECTED);
        g.addEdge("4-5", v4, v5, EdgeType.DIRECTED);
        g.addEdge("5-6", v5, v6, EdgeType.DIRECTED);
        g.addEdge("5-9", v3, v7, EdgeType.DIRECTED);
        g.addEdge("6-10", v3, v7, EdgeType.DIRECTED);
        g.addEdge("10", v3, v7, EdgeType.DIRECTED);
        g.addVertex(v7);

        Layout<Vertice, String> layout = new Layout<Vertice, String>() {
            @Override
            public void initialize() {
            }

            @Override
            public void setInitializer(Transformer<Vertice, Point2D> t) {
            }

            @Override
            public void setGraph(Graph<Vertice, String> graph) {
            }

            @Override
            public Graph<Vertice, String> getGraph() {
                return g;
            }

            @Override
            public void reset() {
            }

            @Override
            public void setSize(Dimension dmnsn) {
            }

            @Override
            public Dimension getSize() {
                return null;
            }

            @Override
            public void lock(Vertice v, boolean bln) {
            }

            @Override
            public boolean isLocked(Vertice v) {
                return true;
            }

            @Override
            public void setLocation(Vertice v, Point2D pd) {
            }

            @Override
            public Point2D transform(Vertice i) {
                Point2D punto = new Point();
                if (i == v1) {
                    punto.setLocation(70, 150);
                } else if (i == v2) {
                    punto.setLocation(120, 100);
                } else if (i == v3) {
                    punto.setLocation(120, 200);
                } else if (i == v4) {
                    punto.setLocation(220, 100);
                } else if (i == v5) {
                    punto.setLocation(220, 200);
                } else if (i == v6) {
                    punto.setLocation(270, 150);
                }else if (i == v7) {
                    punto.setLocation(400, 400);
                }
                return punto;
            }
        };
        layout.setSize(new Dimension(400, 400));

        BasicVisualizationServer<Vertice, String> vv
                = new BasicVisualizationServer(layout);
        vv.setPreferredSize(new Dimension(500, 500));

        Transformer<Vertice, Paint> vertexPaint = new Transformer() {
            @Override
            public Object transform(Object i) {
                return Color.blue;
            }
        };
        
        vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderContext().setEdgeLabelTransformer(new Transformer<String, String>() {
            @Override
            public String transform(String i) {
                return i;
            }
        });
        vv.getRenderContext().setEdgeFontTransformer(new Transformer<String, Font>() {
            @Override
            public Font transform(String i) {
                return new java.awt.Font("Courier New", 0, 14);
            }
        });
        vv.getRenderContext().setEdgeDrawPaintTransformer(new Transformer<String, Paint>() {
            @Override
            public Paint transform(String i) {
                return new Color(255);
            }
        });

        JFrame frame = new JFrame("Simple Graph View");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(vv);
        frame.pack();
        frame.setVisible(true);
    }
}

class Vertice {

    String etiqueta = "";

    public Vertice(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    @Override
    public String toString() {
        return this.etiqueta;
    }
}
