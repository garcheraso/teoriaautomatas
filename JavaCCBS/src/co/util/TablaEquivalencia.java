/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.util;

import java.util.HashMap;

/**
 *
 * @author Ing Belisario
 */
public class TablaEquivalencia {
    public static HashMap<String, String> reservadas=new HashMap<String, String>();
    static{
        reservadas.put("import", "require");
    }
    public static String getEquivalenciaPhp(String reservadaJava) throws Exception{
        String php=reservadas.get(reservadaJava);
        if (php==null){
            throw new Exception("\""+reservadaJava +"\" sin Equivalencia Php");
        }
        return php;
    }
}
