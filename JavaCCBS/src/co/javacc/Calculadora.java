/* Generated By:JavaCC: Do not edit this line. Calculadora.java */
package co.javacc;

public class Calculadora implements co.InterfaceAnalizador, CalculadoraConstants {

    public static void main(String args[]) throws ParseException {
        new Calculadora(System.in).gramatica();
    }

    public void inicio() throws ParseException {
        gramatica();
    }
    public String textoLeido(){
        return "";
    }

    /*
gramatica ::= ( exprFinalizada )+
     */
    static final public void gramatica() throws ParseException {
        label_1:
        while (true) {
            exprFinalizada();
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case NUMERO:
                case 8:
                case 11:
        ;
                    break;
                default:
                    jj_la1[0] = jj_gen;
                    break label_1;
            }
        }
    }

    /*
exprFinalizada ::= expr ";"
     */
    static final public void exprFinalizada() throws ParseException {
        int resultado;
        try {
            resultado = expr();
            jj_consume_token(PUNTOYCOMA);
            System.out.println("=" + resultado);
        } catch (ParseException x) {
            System.out.println(x.toString());
            Token t;
            do {
                t = getNextToken();
            } while (t.kind != PUNTOYCOMA);
        }
    }

    /*
expr ::= term ( ( "+" | "-" ) term )*
     */
    static final public int expr() throws ParseException {
        int acum1 = 0,
                acum2 = 0;
        acum1 = term();
        label_2:
        while (true) {
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case 7:
                case 8:
        ;
                    break;
                default:
                    jj_la1[1] = jj_gen;
                    break label_2;
            }
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case 7:
                    jj_consume_token(7);
                    acum2 = term();
                    acum1 += acum2;
                    break;
                case 8:
                    jj_consume_token(8);
                    acum2 = term();
                    acum1 -= acum2;
                    break;
                default:
                    jj_la1[2] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
        {
            if (true) {
                return acum1;
            }
        }
        throw new Error("Missing return statement in function");
    }

    /*
term ::= fact ( ( "*" | "/" ) fact )*
     */
    static final public int term() throws ParseException {
        int acum1 = 0,
                acum2 = 0;
        acum1 = fact();
        label_3:
        while (true) {
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case 9:
                case 10:
        ;
                    break;
                default:
                    jj_la1[3] = jj_gen;
                    break label_3;
            }
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case 9:
                    jj_consume_token(9);
                    acum2 = fact();
                    acum1 *= acum2;
                    break;
                case 10:
                    jj_consume_token(10);
                    acum2 = fact();
                    acum1 /= acum2;
                    break;
                default:
                    jj_la1[4] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
        {
            if (true) {
                return acum1;
            }
        }
        throw new Error("Missing return statement in function");
    }

    /*
fact ::= NUMERO | "(" expr ")"
     */
    static final public int fact() throws ParseException {
        int acum = 0,
                uMenos = 1;
        switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
            case 8:
                jj_consume_token(8);
                uMenos = -1;
                break;
            default:
                jj_la1[5] = jj_gen;
                ;
        }
        switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
            case NUMERO:
                jj_consume_token(NUMERO);
                 {
                    if (true) {
                        return Integer.parseInt(token.image) * uMenos;
                    }
                }
                break;
            case 11:
                jj_consume_token(11);
                acum = expr();
                jj_consume_token(12);
                 {
                    if (true) {
                        return acum * uMenos;
                    }
                }
                break;
            default:
                jj_la1[6] = jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        throw new Error("Missing return statement in function");
    }

    static private boolean jj_initialized_once = false;
    /**
     * Generated Token Manager.
     */
    static public CalculadoraTokenManager token_source;
    static SimpleCharStream jj_input_stream;
    /**
     * Current token.
     */
    static public Token token;
    /**
     * Next token.
     */
    static public Token jj_nt;
    static private int jj_ntk;
    static private int jj_gen;
    static final private int[] jj_la1 = new int[7];
    static private int[] jj_la1_0;

    static {
        jj_la1_init_0();
    }

    private static void jj_la1_init_0() {
        jj_la1_0 = new int[]{0x920, 0x180, 0x180, 0x600, 0x600, 0x100, 0x820,};
    }

    /**
     * Constructor with InputStream.
     */
    public Calculadora(java.io.InputStream stream) {
        this(stream, null);
    }

    /**
     * Constructor with InputStream and supplied encoding
     */
    public Calculadora(java.io.InputStream stream, String encoding) {
        if (jj_initialized_once) {
            System.out.println("ERROR: Second call to constructor of static parser.  ");
            System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
            System.out.println("       during parser generation.");
            throw new Error();
        }
        jj_initialized_once = true;
        try {
            jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
        } catch (java.io.UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        token_source = new CalculadoraTokenManager(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    /**
     * Reinitialise.
     */
    static public void ReInit(java.io.InputStream stream) {
        ReInit(stream, null);
    }

    /**
     * Reinitialise.
     */
    static public void ReInit(java.io.InputStream stream, String encoding) {
        try {
            jj_input_stream.ReInit(stream, encoding, 1, 1);
        } catch (java.io.UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        token_source.ReInit(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    /**
     * Constructor.
     */
    public Calculadora(java.io.Reader stream) {
        if (jj_initialized_once) {
            System.out.println("ERROR: Second call to constructor of static parser. ");
            System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
            System.out.println("       during parser generation.");
            throw new Error();
        }
        jj_initialized_once = true;
        jj_input_stream = new SimpleCharStream(stream, 1, 1);
        token_source = new CalculadoraTokenManager(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    /**
     * Reinitialise.
     */
    static public void ReInit(java.io.Reader stream) {
        jj_input_stream.ReInit(stream, 1, 1);
        token_source.ReInit(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    /**
     * Constructor with generated Token Manager.
     */
    public Calculadora(CalculadoraTokenManager tm) {
        if (jj_initialized_once) {
            System.out.println("ERROR: Second call to constructor of static parser. ");
            System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
            System.out.println("       during parser generation.");
            throw new Error();
        }
        jj_initialized_once = true;
        token_source = tm;
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    /**
     * Reinitialise.
     */
    public void ReInit(CalculadoraTokenManager tm) {
        token_source = tm;
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            jj_la1[i] = -1;
        }
    }

    static private Token jj_consume_token(int kind) throws ParseException {
        Token oldToken;
        if ((oldToken = token).next != null) {
            token = token.next;
        } else {
            token = token.next = token_source.getNextToken();
        }
        jj_ntk = -1;
        if (token.kind == kind) {
            jj_gen++;
            return token;
        }
        token = oldToken;
        jj_kind = kind;
        throw generateParseException();
    }

    /**
     * Get the next Token.
     */
    static final public Token getNextToken() {
        if (token.next != null) {
            token = token.next;
        } else {
            token = token.next = token_source.getNextToken();
        }
        jj_ntk = -1;
        jj_gen++;
        return token;
    }

    /**
     * Get the specific Token.
     */
    static final public Token getToken(int index) {
        Token t = token;
        for (int i = 0; i < index; i++) {
            if (t.next != null) {
                t = t.next;
            } else {
                t = t.next = token_source.getNextToken();
            }
        }
        return t;
    }

    static private int jj_ntk() {
        if ((jj_nt = token.next) == null) {
            return (jj_ntk = (token.next = token_source.getNextToken()).kind);
        } else {
            return (jj_ntk = jj_nt.kind);
        }
    }

    static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
    static private int[] jj_expentry;
    static private int jj_kind = -1;

    /**
     * Generate ParseException.
     */
    static public ParseException generateParseException() {
        jj_expentries.clear();
        boolean[] la1tokens = new boolean[14];
        if (jj_kind >= 0) {
            la1tokens[jj_kind] = true;
            jj_kind = -1;
        }
        for (int i = 0; i < 7; i++) {
            if (jj_la1[i] == jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i = 0; i < 14; i++) {
            if (la1tokens[i]) {
                jj_expentry = new int[1];
                jj_expentry[0] = i;
                jj_expentries.add(jj_expentry);
            }
        }
        int[][] exptokseq = new int[jj_expentries.size()][];
        for (int i = 0; i < jj_expentries.size(); i++) {
            exptokseq[i] = jj_expentries.get(i);
        }
        return new ParseException(token, exptokseq, tokenImage);
    }

    /**
     * Enable tracing.
     */
    static final public void enable_tracing() {
    }

    /**
     * Disable tracing.
     */
    static final public void disable_tracing() {
    }

}
