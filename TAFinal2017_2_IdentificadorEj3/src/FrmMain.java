
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class FrmMain extends javax.swing.JFrame {

    DefaultTableModel modelo = null;

    /**
     * Creates new form FrmMain
     */
    public FrmMain() {
        initComponents();
        txtFunctionKeyReleased(null);
    }

    private void inicializarModelo() {
        modelo = new DefaultTableModel(new String[]{"Cadena", "Estado"}, 0) {
        };
        tblEstados.setModel(modelo);
        tblEstados.setRowHeight(50);
        TableColumnModel modeloColumna = tblEstados.getColumnModel();
        TableColumn columna = modeloColumna.getColumn(0);
        columna.setMaxWidth(70);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        lblExpresion = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtFunction = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEstados = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Analizador Sintáctico");

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 3, 13)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("Un identificador en un lenguaje de programación tiene las siguientes características:\n*Empieza en letra mayuscula o guion bajo y puede ir seguido de letras, digitos o guiones.\n*Los digitos no pueden aparecer mas de 2 veces seguidas.\n(\"a11b12c\" valido......\"a123b1\" invalido\n*Debe tener mínimo 5 caracteres\n*No puede terminar en Guion.");
        jScrollPane3.setViewportView(jTextArea1);

        lblExpresion.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        lblExpresion.setText("Expresión");

        jLabel7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Analizador Sintáctico");

        txtFunction.setFont(new java.awt.Font("Courier New", 0, 18)); // NOI18N
        txtFunction.setText("M25bb A23bn Cc223");
        txtFunction.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFunctionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFunctionKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(txtFunction);

        tblEstados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEstados);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 889, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblExpresion, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExpresion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        setBounds(0, 0, 929, 463);
    }// </editor-fold>//GEN-END:initComponents

    private void txtFunctionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFunctionKeyPressed
    }//GEN-LAST:event_txtFunctionKeyPressed

    private void txtFunctionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFunctionKeyReleased
        analizar();
    }//GEN-LAST:event_txtFunctionKeyReleased

    private void analizar() {
        inicializarModelo();
        Analizador analizador;
        StringBuilder sb = new StringBuilder();
        String textos[] = txtFunction.getText().trim().replaceAll("\\n", " ").
                replaceAll("\\r\\n", " ").split(" ");
        for (String palabra : textos) {
            String fila[] = new String[2];
            if (palabra.trim().isEmpty()) {
                continue;
            }
            String msg = "";
            try {
                InputStream tmp = new ByteArrayInputStream(palabra.getBytes());
                analizador = new Analizador(tmp);
                analizador.empiezaAEvaluarEstaVaina();
                msg = "¡Bien!";
            } catch (ParseException e) {
                msg = e.getMessage();
            } catch (TokenMgrError e) {
                msg = e.getMessage();
            }
            fila[0] = palabra;
            fila[1] = traducir(msg);
            modelo.addRow(fila);
        }
    }

    private String traducir(String msg) {
        msg = msg.replace("Lexical error ", "Error Léxico ");
        msg = msg.replace("at line ", "en línea ");
        msg = msg.replace("column ", "columna ");
        msg = msg.replace("Was expecting one of:", "Se esperaba uno de estos:");
        msg = msg.replace("Was expecting:", "Se esperaba:");
        msg = msg.replace("Encountered: <EOF>", "Se finalizó el texto");
        msg = msg.replace("Encountered", "Se encontró");
        msg = msg.replace("after ", "después");
        return msg;
    }

    public static void main(String args[]) {
        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMain().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblExpresion;
    private javax.swing.JTable tblEstados;
    private javax.swing.JTextPane txtFunction;
    // End of variables declaration//GEN-END:variables
}
